package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	if len(d) != 16 {
		t.Errorf("Expected deck length of 16, but got %v", len(d))
	}

	if d[0] != "Ace of Spades" {
		t.Errorf("Expected first card of Ace of Spades, but got %v", d[0])
	}

	if d[len(d)-1] != "Four of Clubs" {
		t.Errorf("Expected last card of Four of Clubs, but got %v", d[len(d)-1])
	}
}

func TestSaveAndLoadDeck(t *testing.T) {
	// TODO: clean up temp file! (setup/teardown? try/finally?)
	test_file := "_decktesting"
	os.Remove(test_file)

	deck := newDeck()
	deck.saveToFile(test_file)

	loadedDeck := newDeckFromFile(test_file)

	if len(deck) != len("a") {
		t.Errorf("Lenght of loaded deck %v is not the same as the saved one %v", len(loadedDeck), len(deck))
	}

	os.Remove(test_file)
}
